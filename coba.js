$(document).ready(function(){
    // contentType: "application/json;charset=utf-8",
    $("#btn1").click(function(){
        alert("Text: " + $("#test").text());
    });
    $("#btn2").click(function(){
        alert("HTML: " + $("#test").html());
    });
    $("#btn3").click(function(){
        alert("Class Attribute: " + $("#test").attr("class"));
    });

    $("#btn1").click(function(){
        $("#test").text("This is a new paragraph.");
    });
    $("#btn2").click(function(){
        $("#test").html("This is a <b>new</b> paragraph.");
    });
    $("#btn3").click(function(){
        $("#test2").text("Meaning of Life");
    });

    $("#yes").click(function(){
        $("#tab").hide(500);
    });
    $("#no").click(function(){
        $("#tab").show(500);
    });

    $("#get").click(function(){
        $.get("http://localhost:8080/customer/890", function(data, status){
            alert("Data: " + data.custName + "\nStatus: " + status);
        });
    });

    $("#post").click(function(){
    //     $("post").prop("disabled",true);
    //     $.post("http://localhost:8080/customer",
    //     {
    //         custCode: "CUST975357",
    //         custName: "Diana",
    //         address: "Bogor",
    //         birth: "1992-12-12",
    //         defPayment: "cash"
    //     },

        // $.post('http://localhost:8080/customer',
        //     $(this).serialize(),
        //     function( data, textStatus, jQxhr ){
        //         $('#responsepre').html( data );
        //     },
        //     'text'
        // )

        // function(data,status){
        // var json = JSON.parse(data);
        // // alert("Data: " + data + "\nStatus: " + status);
        // }).done(function(){
        // }).fail(function(xhr, textStatus, errorThrown){
        // }).complete(function(){
        //     $("post").prop("disabled",false);
        // });

        var data1 = {}
        data1["custCode"] = "CUST975369";
        data1["custName"] = "Diana";
        data1["address"] = "Bogor";
        data1["birth"] = "1992-12-12";
        data1["defPayment"] = "cash";

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "http://localhost:8080/customer",
            data: JSON.stringify(data1),
            dataType: 'json',
            timeout: 600000,
            success: function (data, status) {
                // $("#post").prop("disabled", false);
                alert("Input data berhasil dengan kode: " + data1.custCode + "\nStatus: " + status);
            },
            error: function (e) {
                // $("#post").prop("disabled", false);
                alert("Input data gagal dengan kode: " + data1.custCode + "\nStatus: " + status);
                //...
            }
        });
    });
});


// data = {            
//     custCode: "CUST975357",
//     custName: "Diana",
//     address: "Bogor",
//     birth: "1992-12-12",
//     defPayment: "cash"
// }

$(document).ready(function(){
    
});